def ruler(number):
    for i in range(1, number + 1):
        print(i % 10, end = '')

# To check the correctness of the function
# HARDCODED as mentioned

print('5 - > ', end = '')
ruler(5)

print('\n10 - > ', end = '')
ruler(10)

print('\n25 - > ', end = '')
ruler(25)

print('\n51 - > ', end = '')
ruler(51)

print('\n80 - > ', end = '')
ruler(80)