def isListOfInts(data):
    d = True
    try:
        if type(data) != list:
            raise ValueError

        if len(data) == 0:
            d = True
        else:    
            for i in data:
                if isinstance(i, int) == False:
                    d =  False
        
        return d
    
    except ValueError:
        return 'ValueError: ' + str(data) + ' - arg not of <list> type'

def testList(value):
    d = isListOfInts(value)
    if d != True and d!= False:
        print(d)
    else:
        print(value, '-->', d)

# Testing the correctness of the function as mentioned
testList([])
testList([1])
testList([1,2])
testList([0])
testList(['1'])
testList([1,'a'])
testList(['a',1])
testList([1, 1.])
testList([1., 1.])
testList((1,2))