import sys

def isWhiteLine(string):
    for i in string:
        if (i >= 'A' and i <= 'Z') or (i >= 'a' and i <= 'z'):
            return string 
    return True

with open(sys.argv[1], 'r+') as file:
    fp = file.readlines()
    # print(fp)

    for i in fp:
        temp = isWhiteLine(i.strip('\n')) 
        if temp == True:
            continue
        print(temp)
